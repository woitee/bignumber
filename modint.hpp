#ifndef MODINT_HPP_
#define MODINT_HPP_

#include <vector>
#include <tuple>
#include "bigint.hpp"

class modint {
public:
	typedef long long signed_int;
	typedef unsigned long long used_int;
private:
	typedef std::vector<used_int> vec_t;
	typedef std::shared_ptr<vec_t> vecp_t;

	// not actually need to be prime, pairwise coprime would do
	// all primes have to fit in an unsigned integer half the size of used_int
	static const used_int primes[]; // initialized in modint.cpp
	static const size_t nprimes;    // initialized in modint.cpp
	// for faster conversion of modint to bigint
	static const used_int helpconsts[]; // initialized in modint.cpp

	vecp_t vecp;
	void ensure_unique();

	typedef std::tuple<signed_int, signed_int, signed_int> triint;
	static triint ext_euclid(signed_int a, signed_int b);
public:
	static std::vector<used_int> gen_helpconsts(bool print = true);

	// STANDARD USAGE
	static const modint ONE;
	static const modint ZERO;

	modint();
	explicit modint(const used_int& a);
	explicit modint(const bigint& b);

	modint & operator += (const modint& m);
	modint operator + (const modint& m) const;
	modint & operator -= (const modint& m);
	modint operator - (const modint& m) const;
	modint & operator *= (const modint& m);
	modint operator * (const modint& m) const;

	modint & operator++ ();
	modint operator++ (int);
	modint & operator-- ();
	modint operator-- (int);

	bool operator== (const modint& m) const;
	bool operator!= (const modint& m) const;

	friend std::ostream & operator<< (std::ostream & out, const bigint & b);
	friend std::istream & operator>> (std::istream & in, bigint & b);

	operator bigint () const;
};

#endif