#include <iostream>
#include "bigint.hpp"
#include "modint.hpp"
#include <ctime>

void test(bool b, std::string s) {
	if (b) {
		std::cout << "SUCCESS: " << s << std::endl;
	} else {
		std::cout << "!!! FAIL !!! : " << s << std::endl;
	}
}

void test_bigint() {
	bigint a, b, c, d;
	std::string s;
	
	// testing
	a = 3;
	s = a.to_string();
	test(s == "3", "simple string");

	a = 2000000000;
	s = a.to_string();
	test(s == "2000000000", "long string");
	
	a = 123456789;
	a = a * a;
	s = a.to_string();
	test(s == "15241578750190521", "very long string, with multiplication");

	a = bigint();
	s = a.to_string();
	test(s == "0", "default constructor");

	a = bigint(123456);
	s = a.to_string();
	test(s == "123456", "used_int constructor");

	std::cout << "---OPERATION BLOCK---" << std::endl;

	a = 123456789;
	b = 987654321;
	a |= b;
	s = a.to_string();
	test(s == "1071639989", "bitwise or");

	a = 123456789;
	b = 987654321;
	a &= b;
	s = a.to_string();
	test(s == "39471121", "bitwise and");

	a = 123456789;
	a += 999999999;
	s = a.to_string();
	test(s == "1123456788", "addition");

	a -= 123456789;
	s = a.to_string();
	test(s == "999999999", "subtraction");

	a = 987654321;
	a *= 999999999;
	s = a.to_string();
	test(s == "987654320012345679", "multiplication");

	a = 999999999;
	a *= 987654321;
	a *= 123456789;
	b = a / 5762;
	s = b.to_string();
	test(s == "21161511799844262042236", "short_division");

	b = a % 5762;
	s = b.to_string();
	test(s == "899", "short_modulus");

	a = 65536*2;
	b = 65536;
	c = a / b;
	s = c.to_string();
	test(s == "2", "basic long division");

	a = 999999999;
	a *= 987654321;
	a *= 123456789;
	b = a;
	a *= 987654321;
	a += 7984321;
	c = a / b;
	s = c.to_string();
	test(s == "987654321", "long_division");
	c = a % b;
	s = c.to_string();
	test(s == "7984321", "long_modulus");

	a = 6541238;
	a *= 654987;
	a *= 654987;
	a += 123564;
	b = 12356;
	b *= 874;
	b *= 996610;
	b += 512;
	c = a / b;
	s = c.to_string();
	test(s == "260741", "long_division2");

	a = 123456789;
	a <<= 29;
	s = a.to_string();
	test(s == "66280358903021568", "binary shift left");

	a >>= 29;
	s = a.to_string();
	test(s == "123456789", "binary shift right");

	std::cout << "---COMPARATOR BLOCK---" << std::endl;

	a = 1;
	b = 1;
	test((a == b), "small_equality");

	a = 1;
	b = 2;
	test(!(a == b), "small_inequality");

	c = 123456789;
	c = c * c;
	d = c;
	test((c == d), "big equality");

	d = d + 1;
	test(!(c == d), "big inequality");

	test((d > c), "big bigger than");
	test((c < d), "big less than");

	std::cout << "---ADDED OPERATIONS---" << std::endl;
	
	std::istringstream iss("123456789123456789");
	iss >> a;
	test (a.to_string() == "123456789123456789", "parse from string");
}

// this test assumes bigint is working correctly
void test_modint() {
	std::cout << "----- MODINT TESTS -----" << std::endl;
	modint m(999999999999);
	bigint a = 999999999999;
	bigint b = m;
	test(a == b, "converting smaller modint to bigint");

	b = 999999999999;
	b *= b; b *= b;
	m = modint(b);
	a = m;
	test(a == b, "converting larger modint to bigint");

	m += m;
	b += b;
	a = m;
	test(a == b, "adding modints");

	modint m2;
	a = 123456789123456; a = a*a;
	b = 66874559878981;
	m = modint(a); m2 = modint(b);
	a = a-b;
	m = m-m2;
	b = m;
	test(a == b, "subtracting modints");

	a = 123456789123456; a = a*a;
	b = 66874559878981;
	m = modint(a); m2 = modint(b);
	a = a*b;
	m = m*m2;
	b = m;
	test(a == b, "multiplying modints");

	a = 123456789654897;
	m = modint(a);
	++a;
	++m;

	b = m;
	test(a == b, "pre-increment modint");

	a++; m++;
	b = m;
	test(a == b, "post-increment modints");

	m = modint(12345698798741);
	m2 = m;
	++m; --m;
	test(m == m2, "equality");
	++m2;
	test(m != m2, "unequality");

	test(--m2 == m, "pre-decrement");
	test(m2-- == m, "post-decrement");
}

// Test of computing 1000 factorial, using the templated type
// modint rates slower than bigint, because multiplying small numbers
// is faster in bigint (modint has constant time multiplication)
template <typename T>
T test_factorial(int factmax = 1000, std::string title = "UNDEFINED") {
	std::cout << "-- " << title << " FACTORIAL BENCHMARK --" << std::endl;
	std::clock_t c0 = std::clock();

	T one, a, ret;
#ifdef _DEBUG
	const int repeat_count = 10;
#else
	const int repeat_count = 1000;
#endif

	for (int i = 0; i < repeat_count; ++i) {
		one = T(1);
		a = one;
		ret = a;
		for (int i = 2; i <= factmax; ++i) {
			a = a + one;
			ret *= a;
		}
	}

	std::clock_t c1 = std::clock();

	std::cout << "Time total: " << (double)1000 * (c1-c0) / CLOCKS_PER_SEC << "ms" << std::endl;

	return ret;
}

// Test of multiplying two numbers, using the templated type
// no conversion is taken into account, just the multiplication
// when tested with large numbers, such as 500!, modint is considerably faster
template <typename T>
void test_multiplication(T a, T b, std::string title) {
	std::cout << "--" << title << " MULTIPLICATION BENCHMARK--" << std::endl;

#ifdef _DEBUG
	const int repeat_count = 100;
#else
	const int repeat_count = 10000;
#endif

	T c;

	std::clock_t c0 = std::clock();

	for (int i = 0; i < repeat_count; ++i)
		c = a * b;
	
	std::clock_t c1 = std::clock();

	std::cout << "Time total: " << (double)1000 * (c1-c0) / CLOCKS_PER_SEC << "ms" << std::endl;
	
}

int main (int argc, char ** argv){
	test_bigint();
	test_modint();
	std::cout << std::endl;
	
	bigint b = test_factorial<bigint>(500, "BIGINT");
	std::cout << std::endl;
	modint m = test_factorial<modint>(500, "MODINT");
	std::cout << std::endl;

	test_multiplication(b, b, "BIGINT");
	std::cout << std::endl;
	test_multiplication(m, m, "MODINT");
	std::cout << std::endl;

	return 0;
}