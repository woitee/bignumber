#include "bigint.hpp"

typedef bigint::used_int used_int;

const size_t bigint::biggestDigits = (size_t)floor(log10(bottomMask));
const used_int bigint::biggestDec = (used_int)pow(10, biggestDigits);

// default constructor with zero value
bigint::bigint() : vecp (new vec_t(1)) {}

// constructor of small numbers
bigint::bigint(const used_int a) : vecp (new vec_t(2)) {
	(*vecp)[0] = a & bottomMask;
	(*vecp)[1] = a >> halfSize;
	if ((*vecp)[1] == 0) {
		vecp->pop_back();
	}
}

void bigint::ensure_unique() {
	if (!vecp.unique()) {
		vecp = vecp_t (new vec_t(*vecp));
	}
}
void bigint::normalize() {
	size_t a;
	for (a = vecp->size() - 1; a > 0; --a) {
		if ((*vecp)[a] != 0) { break; }
	}
	vecp->resize(a+1);
}

used_int bigint::to_int() const {
	if (vecp->size() >= 3) {
		throw std::bad_cast();
	} else if (vecp->size() >= 2) {
		return (*vecp)[0] + ((*vecp)[1] << halfSize);
	} else if (vecp->size() >= 1) {
		return (*vecp)[0];
	} else {
		return 0;
	}
}

// operations
bool bigint::operator== (const bigint & other) const{
	if (this->vecp->size() != other.vecp->size()) {
		return false;
	}
	for (auto it = vecp->begin(), it2 = other.vecp->begin(); it != vecp->cend(); ++it, ++it2) {
		if (*it != *it2) {
			return false;
		}
	}
	return true;
}
bool bigint::operator!= (const bigint & other) const{
	return ! (*this == other);
}
bool bigint::operator> (const bigint & other) const{
	if (this->vecp->size() > other.vecp->size()) {
		return true;
	}
	if (this->vecp->size() < other.vecp->size()) {
		return false;
	}
	for (auto it = vecp->rbegin(), it2 = other.vecp->rbegin(); it != vecp->rend(); ++it, ++it2) {
		if (*it > *it2) {
			return true;
		} else if (*it < *it2) {
			return false;
		}
	}
	// numbers are equal
	return false;
}
bool bigint::operator< (const bigint & other) const{
	return other > *this;
}
bool bigint::operator>= (const bigint & other) const{
	return ! this->operator< (other);
}
bool bigint::operator<= (const bigint & other) const{
	return ! this->operator> (other);
}

bigint & bigint::operator|= (const bigint & other) {
	ensure_unique();
	size_t len = vecp->size() < other.vecp->size() ? vecp->size() : other.vecp->size();
	for (size_t i = 0; i < len; ++i) {
		(*vecp)[i] |= (*other.vecp)[i];
	}
	normalize();
	return *this;
}
bigint bigint::operator| (const bigint & other) const {
	bigint ret = (*this);
	ret |= other;
	return ret;
}
bigint & bigint::operator&= (const bigint & other) {
	ensure_unique();
	size_t len = vecp->size() < other.vecp->size() ? vecp->size() : other.vecp->size();
	for (size_t i = 0; i < len; ++i) {
		(*vecp)[i] &= (*other.vecp)[i];
	}
	normalize();
	return *this;
}
bigint bigint::operator& (const bigint & other) const {
	bigint ret = (*this);
	ret &= other;
	return ret;
}
bigint & bigint::operator^= (const bigint & other) {
	ensure_unique();
	size_t len = vecp->size() < other.vecp->size() ? vecp->size() : other.vecp->size();
	for (size_t i = 0; i < len; ++i) {
		(*vecp)[i] ^= (*other.vecp)[i];
	}
	normalize();
	return *this;
}
bigint bigint::operator^ (const bigint & other) const {
	bigint ret = (*this);
	ret ^= other;
	return ret;
}

bigint & bigint::operator<<= (const bigint & other) {
	ensure_unique();
	if (*this == 0) { return (*this); }
	// throws exception if other is too large
	// vector indexing type would overflow anyway
	used_int oth = other.to_int();
	vec_t & vec = *vecp;
	vec.insert(vec.begin(), static_cast<size_t>(oth / halfSize), 0);
	vec.push_back(0);
	used_int c = 0;
	for (used_int & vi : *vecp) {
		vi <<= oth % halfSize;
		vi |= c;
		c = vi >> halfSize;
		vi &= bottomMask;
	}
	if (vec.back() == 0) { vec.pop_back(); }
	return *this;
}
bigint bigint::operator<< (const bigint & other) const {
	bigint ret = *this;
	ret <<= other;
	return ret;
}
bigint & bigint::operator>>= (const bigint & other) {
	ensure_unique();
	// if other is too large, return 0
	// because it would reduce any bigint to 0
	if (other > SIZE_MAX) {
		return (*this) = bigint(0);
	}
	used_int oth = other.to_int();
	vec_t & vec = *vecp;
	if (oth / halfSize >= vec.size()) {
		return (*this) = bigint(0);
	}
	vec_t::iterator it = vec.begin();
	// static cast to size_t should not cause loss of data, due to max bigint size limited by vector size
	vec_t::iterator it2 = it + static_cast<size_t>(oth / halfSize);
	vec.erase(it, it2);
	used_int shift = oth % halfSize;
	used_int mask = (1 << shift) - 1;
	used_int c = 0, c2;
	for (auto rit = vec.rbegin(); rit != vec.rend(); ++rit) {
		c2 = (*rit) & mask;
		(*rit) >>= shift;
		c <<= halfSize - shift;
		(*rit) |= c;
		c = c2;
	}
	if (vec.size() > 0 && vec.back() == 0) { vec.pop_back(); }
	return *this;
}
bigint bigint::operator>> (const bigint & other) const {
	bigint ret = *this;
	ret >>= other;
	return ret;
}
bigint & bigint::operator+= (const bigint & other) {
	ensure_unique();
	vec_t & vec = *vecp;
	size_t len = vec.size() > other.vecp->size() ? vec.size() : other.vecp->size();
	vec.resize(len+1); //in case of overflow
	used_int overflow = 0; size_t a;
	for (a = 0; a < other.vecp->size(); ++a) {
		vec[a] = vec[a] + (*other.vecp)[a] + overflow;
		overflow = vec[a] >> halfSize;
		vec[a] &= bottomMask;
	}
	while (overflow > 0) {
		vec[a] += overflow;
		overflow = vec[a] >> halfSize;
		vec[a] &= bottomMask;
		++a;
	}
	if (a <= len) {
		vec.pop_back();
	}
	return *this;
}
bigint bigint::operator+ (const bigint & other) const {
	bigint ret = (*this);
	ret += other;
	return ret;
}
bigint & bigint::operator-= (const bigint & other) {
	ensure_unique();
	vec_t & vec = *vecp;
	size_t a;
	used_int c = 0;
	for (a = 0; a < other.vecp->size(); ++a) {
		if (vec[a] >= (*other.vecp)[a] + c) {
			vec[a] -= (*other.vecp)[a] + c;
			c = 0;
		} else {
			vec[a] += static_cast<used_int>(1) << halfSize;
			vec[a] -= (*other.vecp)[a] + c;
			c = 1;
		}
	}
	while (c > 0) {
		if (vec[a] >= c) {
			vec[a] -= c;
			c = 0;
		} else {
			vec[a] += static_cast<used_int>(1) << halfSize;
			vec[a] -= c;
			c = 1;
		}
		++a;
	}
	normalize();
	return *this;
}
bigint bigint::operator- (const bigint & other) const {
	bigint ret = (*this);
	ret -= other;
	return ret;
}
bigint bigint::operator* (const bigint & other) const {
	bigint ret;
	used_int c, t; size_t i, j;
	vec_t & vec = (*ret.vecp);
	vec_t & vec1 = (*this->vecp), & vec2 = (*other.vecp);
	vec.resize(vec1.size() + vec2.size());
	for (i = 0; i < vec1.size(); ++i) {
		c = 0;
		for (j = 0; j < vec2.size(); ++j) {
			t = vec1[i]*vec2[j] + c;
			vec[i+j] += t;
			c = vec[i+j] >> halfSize;
			vec[i+j] &= bottomMask;
		}
		vec[i+j] = c;
	}
	ret.normalize();
	return ret;
}
bigint & bigint::operator*= (const bigint & other) {
	(*this) = (*this) * other;
	return (*this);
}

// returns pair of quotient and remainder
std::pair<bigint,bigint> bigint::remquo(const bigint & other) const {
	std::pair<bigint, bigint> pair;
	if (other.vecp->size() <= 1) {
		// dividing by single "digit" numbers is considerably simpler
		// and as such is dealt with individually, school-algorithm wise
		used_int c = 0;
		const vec_t & vec = *vecp;
		bigint & quot = pair.first;
		pair.first.vecp = vecp_t(new vec_t(vec.size()));
		vec_t & quotv = *pair.first.vecp;
		used_int oth = (*other.vecp)[0];
		for (size_t i = vec.size(); i-- > 0; ) {
			c |= vec[i];
			quotv[i] = c / oth;
			c %= oth;
			c <<= halfSize;
		}
		c >>= halfSize;
		pair.second = c;
		quot.normalize();
		return pair;
	} else {
		if (this->vecp->size() < other.vecp->size()) {
			pair.first = 0;
			pair.second = *this;
			return pair;
		}
		// Algorithm inspired by one from Donald Knuth's Art of Programming: Volume 2
		// Normalize, we want biggest digit of divisor to be at least base / 2
		used_int d = 1;
		used_int oth_top = *(--other.vecp->end()); //biggest digit of divisor
		while (oth_top << d < (static_cast<used_int>(1) << halfSize) >> 1) {
			++d;
		}
		bigint divden = *this << d;
		bigint divsor = other << d;
		size_t n = divsor.vecp->size();
		used_int last = (*divsor.vecp)[n-1];
		used_int prelast = (*divsor.vecp)[n-2];
		pair.first.vecp->resize(divden.vecp->size() - n);
		vec_t & vec = *divden.vecp;

		for (size_t j = divden.vecp->size() - n; j-- > 0; ) {
			// Calculate q^
			// This q is not correct, but it can be at most 2 higher than supposed to
			used_int q = (vec[j+n] << halfSize) + vec[j+n-1];
			used_int r = q % last;
			q /= last;
			while ((q == static_cast<used_int>(1) << halfSize) || (q*prelast > (static_cast<used_int>(1) << halfSize)*r + vec[j+n-2])) {
				--q;
				r += last;

				if (r >= static_cast<used_int>(1) << halfSize) { break; }
			}
			// Multiply and subtract
			// Test to correct the value of q (unlikely now, but still can be 1 higher)
			bigint subtrahend = (divsor * q) << (halfSize * j);
			if (subtrahend > divden) {
				--q;
				subtrahend -= divsor << (halfSize * j);
			}
			(*pair.first.vecp)[j] = q;
			divden -= subtrahend;
		}
		pair.second = divden >>= d;
		return pair;
	}
}
bigint bigint::operator/ (const bigint & other) const {
	return remquo(other).first;
}
bigint & bigint::operator/= (const bigint & other) {
	(*this) = remquo(other).first;
	return *this;
}
bigint bigint::operator% (const bigint & other) const {
	return remquo(other).second;
}
bigint & bigint::operator%= (const bigint & other) {
	(*this) = remquo(other).second;
	return *this;
}

bigint & bigint::operator++ (){
	// prefix increment
	return (*this) += 1;
}
bigint bigint::operator++ (int){
	bigint ret = *this;
	(*this) += 1;
	return ret;
}
bigint & bigint::operator-- (){
	// prefix decrement
	return (*this) -= 1;
}
bigint bigint::operator-- (int){
	bigint ret = *this;
	(*this) -= 1;
	return ret;
}

std::string bigint::to_string() const {
	std::string tmp;
	std::deque<char> ret;
	auto pair = this->remquo(biggestDec);
	while (pair.first != 0) {
		tmp = std::to_string((*pair.second.vecp)[0]);
		ret.insert(ret.begin(), tmp.begin(), tmp.end());
		tmp = std::string(biggestDigits - tmp.size(), '0');
		ret.insert(ret.begin(), tmp.begin(), tmp.end());
		
		pair = pair.first.remquo(biggestDec);
	}
	tmp = std::to_string((*pair.second.vecp)[0]);
	ret.insert(ret.begin(), tmp.begin(), tmp.end());

	return std::string(ret.begin(), ret.end());
}

std::ostream & operator<< (std::ostream & out, const bigint & b) {
	return out << b.to_string();
}
std::istream & operator>> (std::istream & in, bigint & b) {
	//ToDo
	b = 0;
	std::stringstream ss;
	used_int i;
	while (true) {
		ss.clear();
		for (i = 0; i < bigint::biggestDigits; ++i) {
			char c = in.get();
			if (c == -1 || isspace(c)) {
				break;
			}
			ss << c;
		}
		b *= (used_int)pow(10, i);
		used_int x;
		ss >> x;
		if (ss.fail()) {
			ss.setstate(std::ios::failbit);
			return in;
		}
		b += x;
		if (i < bigint::biggestDigits) {
			return in;
		}
	}
	// return in;
}
