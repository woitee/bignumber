#ifndef bigint_HPP_
#define bigint_HPP_

#include <vector>
#include <deque>
#include <iostream>
#include <memory>
#include <cmath>
#include <cctype>
#include <string>
#include <sstream>
#include <ios>



class bigint{
public:
	// this typedef can be used to simply choose between 32-bit and 64-bit versions
	// set it to an 32-bit unsigned integer to compile 32-bit version, etc.
	typedef unsigned long long used_int;
private:
	typedef std::vector<used_int> vec_t;
	typedef std::shared_ptr<vec_t> vecp_t;
	vecp_t vecp; //only data field

	// *** CONSTANTS ***

	// half the size of used_int in bits 
	static const size_t halfSize = sizeof(used_int) << 2;
	// ones in bottom bits, zeros in upper
	static const used_int bottomMask = ((used_int)1 << halfSize) - 1;
	// highest power of ten fitting in halfSize bits
	// initialized in bigint.cpp
	static const used_int biggestDec;
	// count of zeros in biggestDec
	// also inititalized in bigint.cpp
	static const size_t biggestDigits;

	// *** FUNCTIONS ***

	void ensure_unique();
	void normalize();
public:
	// *** CONSTRUCTORS ***
	bigint();
	bigint(const used_int a);

	// *** OPERATIONS ***
	std::string to_string() const;
	used_int to_int() const;
	//this returns remainder, and sets quot to quotient
	std::pair<bigint, bigint> remquo(const bigint & b) const;

	// *** OPERATORS ***

	bool operator== (const bigint & b) const;
	bool operator!= (const bigint & b) const;
	bool operator<= (const bigint & b) const;
	bool operator>= (const bigint & b) const;
	bool operator<  (const bigint & b) const;
	bool operator>  (const bigint & b) const;

	bigint operator| (const bigint & b) const;
	bigint & operator|= (const bigint & b);
	bigint operator& (const bigint & b) const;
	bigint & operator&= (const bigint & b);
	bigint operator^ (const bigint & b) const;
	bigint & operator^= (const bigint & b);
	bigint operator<< (const bigint & b) const;
	bigint & operator<<= (const bigint & b);
	bigint operator>> (const bigint & b) const;
	bigint & operator>>= (const bigint & b);

	bigint operator+ (const bigint & b) const;
	bigint & operator+= (const bigint & b);
	bigint operator- (const bigint & b) const;
	bigint & operator-= (const bigint & b);
	bigint operator* (const bigint & b) const;
	bigint & operator*= (const bigint & b);
	bigint operator/ (const bigint & b) const;
	bigint & operator/= (const bigint & b);
	bigint operator% (const bigint & b) const;
	bigint & operator%= (const bigint & b);

	bigint & operator++ ();
	bigint operator++ (int);
	bigint & operator-- ();
	bigint operator-- (int);

	friend std::ostream & operator<< (std::ostream & out, const bigint & b);
	friend std::istream & operator>> (std::istream & in, bigint & b);
};

#endif